const express = require("express");
const imap = require(__dirname + "/mails/mails-imap.js");
const smtp = require(__dirname + "/mails/mails-smtp.js");
const jwt = require('./helpers/jwt.js');


// middlewares
const logger = require("morgan");
const serveStatic = require("serve-static");
const favicon = require("serve-favicon");
const bodyParser = require("body-parser");
const PORT = 80;
const app = express();

app.use(favicon(__dirname + "/app/favicon.ico"));
app.use(logger(":method :url")); //logs the method called and the url
app.use(serveStatic(__dirname + "/app"));
app.use(bodyParser.json());
//app.use(jwt());

// API

const api = express();

// __________________ LOGIN _______________________

const crud = require('./users/crud.js');

// routes
api.post('/users/authenticate', crud.authenticate);
api.post('/users/register', crud.register);
api.get('/users', crud.getAll);
api.get('/users/current', crud.getCurrent);
api.get('/users/:id', crud.getById);
api.delete('/users/:id', crud._delete);

module.exports = api;


// __________________ MAILS _______________________
// POST /api/send
api.post("/mails/send", smtp.sendMail);

// api.use(serviceRecupMails.connexion);

// Récupérer la liste des folders
// GET /api/folders
api.get("/mails/folders", imap.simpleConnection);
api.get("/mails/folders", imap.getFolders);

// Récupérer un dossier
// GET /api/folders/idDossier
api.get("/mails/folders/:idDossier", imap.folderConnection);
api.get("/mails/folders/:idDossier", imap.getFolder);

// Récupérer un mail
// GET /api/folders/idDossier/idMail
api.get("/mails/folders/:idDossier/:idMail", imap.folderConnection);
api.get("/mails/folders/:idDossier/:idMail", imap.getMail);


app.use("/api", api);

const server = app.listen(PORT, function () {
    console.log('Server listening on port ' + PORT);
});

const io = require("socket.io")(server);

imap.setIo(io);