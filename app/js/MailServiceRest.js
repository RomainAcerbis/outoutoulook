angular.module("MailServiceRest", [ "ngResource" ])
.factory("mailService", function($resource) {
	
	var URL_API = "http://localhost/api/mails/";

	var serviceRest = $resource(URL_API + "folders", null,
		{ 
			"getfolders" : { method: "GET", isArray: true },
			"getDossier" : { method: "GET", isArray: false, url: URL_API + "folders/:idDossier" },
			"getMail" : { method: "GET", isArray: false, url: URL_API + "folders/:idDossier/:idMail" },
			"sendMail" : { method: "POST", url: URL_API + "send" }
		});

	return {
		getfolders: function() {
			return serviceRest.getfolders();
		},
		getDossier: function(valDossier) {
			return serviceRest.getDossier({idDossier: valDossier});
		},
		getMail: function(valDossier, idMail) {
			return serviceRest.getMail({idDossier: valDossier, idMail: idMail});
		},
		sendMail: function(mail) {
			serviceRest.sendMail(mail, function() {
				alert("Le mail a bien été envoyé !")
			}, function(response) {
				alert("Erreur " + response.status + " lors de l'send de mail : " + response.data);
			});
			
		}
	};

})