const propertiesReader = require("properties-reader");

const props = propertiesReader(__dirname + "/.properties");

const smtp = require("emailjs");

const getConnection = () => {
	return new smtp.SMTPClient({
		user: props.get("smtp.user"),
		password: props.get("smtp.password"),
		host: props.get("smtp.host"),
		port: props.get("smtp.port"),
		ssl: props.get("smtp.secure")
	});
}

exports.sendMail = function(req, res) {
	const email = req.body;

	const connection = getConnection();
	connection.send({
		from: props.get("email.name") + " <" + props.get("email.address") + ">",
		to: email.to,
		subject: email.subject,
		text: email.content,
		attachment: [
			{ data : email.content, alternative: true }
		]
	}, function(error, result) {
		if (error) {
			res.status(500).send("Une erreur s'est produite lors de l'envoi de l'e-mail : " + error);
		} else {
			res.send({ succes: true, email: email });
		}
	});
}