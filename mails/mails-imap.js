const Imap = require("browserbox");
const propertiesReader = require("properties-reader");
const MailParser = require("mailparser").MailParser;
const Promise = require("es6-promise").Promise;
const props = propertiesReader(__dirname + "/.properties");
let simpleConnection = null;
const foldersConnections = {};
let io = null;

exports.setIo = obj => io = obj;

const formatNameMail = users => {
	return users.map(user => {
		return user.name ? user.name : user.address
	}).join(", ");
}

const getAllFolders = folders => {
	let allFolders = [];

	folders.forEach(item => {
		const path = item.path;
		const name = item.name == "INBOX" ? "Boite de réception" : item.name;
		allFolders.push({
			label: name,
			value: path
		});
		if (item.children && item.children.length > 0) {
			allFolders = allFolders.concat(getAllFolders(item.children));
		}
	});

	return allFolders;
}

const getConnection = (success, error) => {
	const imap = new Imap(props.get("imap.host"), props.get("imap.port"), {
		auth: {
			user: props.get("imap.user"),
			pass: props.get("imap.password")
		},
		useSecureTransport: props.get("imap.secure")
	});

	imap.connect();

	imap.onauth = success;

	imap.onerror = error;

	imap.onclose = () => console.log("Fin de la connexion");

	return imap;
}

exports.simpleConnection = (req, res, next) => {
	if (simpleConnection && simpleConnection.authenticated) {
		console.log("Connexion existante");
		req.connexionImap = simpleConnection;
		next();
	} else {
		console.log("Connexion à créer");
		const connection = getConnection(() => {
			req.connexionImap = connection;
			simpleConnection = connection;
			next();
		}, error => {
			res.status(503).send("Impossible de se connecter au serveur IMAP : " + error);
			connection.close();
		});
	}

}

exports.folderConnection = (req, res, next) => {
	const idFolder = req.params.idDossier;
	const folderConnection = foldersConnections[idFolder];

	if (folderConnection && folderConnection.authenticated && folderConnection.selectedMailbox == idFolder) {
		console.log("Connexion dossier existante");
		req.connexionImap = folderConnection;
		next();
	} else {
		const connection = getConnection(() => {

			connection.selectMailbox(idFolder, { readOnly: true }).then(info => {

				connection.onupdate = (type, value) => {
					if (type == "exists") {
						info.exists = value;

						io.emit("newmail", idFolder);
					} else if (type == "expunge") {
						info.exists--;
					}
				};

				connection.mailbox = info;
				req.connexionImap = connection;
				foldersConnections[idFolder] = connection;
				next();
			}, error => {
				res.status(404).send("Le dossier demandé est introuvable : " + error);
				connection.close();
			})

		}, error => {
			res.status(503).send("Impossible de se connecter au serveur IMAP : " + error);
			connection.close();
		});
	}
}

exports.getFolders = (req, res) => {
	const imap = req.connexionImap;

	imap.listMailboxes().then(mailboxes => {
		const folders = getAllFolders(mailboxes.children);
		res.send(folders);

	}, error => {
		console.log("Erreur dans la récupération des folders : " + error);
	})

}

exports.getFolder = (req, res) => {
	const imap = req.connexionImap;
	const idFolder = req.params.idDossier;

	if (imap.mailbox.exists == 0) {
		res.send({ value: idFolder, emails: [] });
		return;
	}

	imap.listMessages("1:*", [ "uid", "flags", "envelope" ]).then(messages => {
		const emails = messages.map(msg => {
			return {
				id: msg.uid,
				from: formatNameMail(msg.envelope.from),
				to: formatNameMail(msg.envelope.to),
				subject: msg.envelope.subject,
				date: new Date(msg.envelope.date)
			};
		})

		res.send({ value: idFolder, emails: emails });

	}, error => {
		res.status(500).send("Une erreur s'est produite lors de la récupération des mails : " + error);
		imap.close();
	});
}



exports.getMail = (req, res) => {
	const imap = req.connexionImap;
	const idMail = req.params.idMail;
	imap.listMessages(idMail, [ "uid", "flags", "envelope", "bodystructure", "body[]" ], { byUid: true }).then(messages => {
		const msg = messages[0];

		const mp = new MailParser();
		console.log(msg);

		mp.on("data", emailParse => {

			res.send({
				id: msg.uid,
				from: formatNameMail(msg.envelope.from),
				to: formatNameMail(msg.envelope.to),
				subject: msg.envelope.subject,
				date: new Date(msg.envelope.date),
				content: emailParse.html ? emailParse.html : emailParse.text
			});
		})
		mp.write(msg['body[]']);
		mp.end();			
	}, error => {
		res.status(500).send("Une erreur s'est produite lors de la récupération des mails : " + error);
	});
}